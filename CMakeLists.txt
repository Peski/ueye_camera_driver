cmake_minimum_required(VERSION 2.8.3)
project(ueye_camera_driver CXX)

## CMake config
list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake_modules")

## Set CXX optimization flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wpedantic -Wall -Wextra -pthread")

## Set build type to Release by default
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Build type" FORCE)
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release")
endif(NOT CMAKE_BUILD_TYPE)
message(STATUS "Build type: " ${CMAKE_BUILD_TYPE})

if(CMAKE_BUILD_TYPE STREQUAL "Release")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -DNDEBUG")
else(CMAKE_BUILD_TYPE STREQUAL "Release")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -g")
  set(MRPT_DONT_USE_DBG_LIBS ON)
endif(CMAKE_BUILD_TYPE STREQUAL "Release")
message(STATUS "Flags: " ${CMAKE_CXX_FLAGS})

## Dependencies
find_package(catkin REQUIRED COMPONENTS
  roscpp sensor_msgs image_transport cv_bridge nodelet dynamic_reconfigure
)

# TODO Find uEye SDK
#find_package(Boost REQUIRED filesystem system)
find_package(OpenCV REQUIRED)

# TODO For debug only
find_package(yaml-cpp REQUIRED)

generate_dynamic_reconfigure_options(
  cfg/uEye.cfg
)

catkin_package(CATKIN_DEPENDS
  roscpp sensor_msgs nodelet
)

include_directories(include ${catkin_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIRS} ${YAML_CPP_INCLUDE_DIRS})

message(STATUS "catkin_LIBRARIES: " ${catkin_LIBRARIES})

## Header files
file(GLOB_RECURSE HEADER_FILES include/*.hpp include/*.h)
add_custom_target(header_files SOURCES ${HEADER_FILES})

## Link executables
add_library(uEyeCamera src/uEyeCamera.cpp)
target_link_libraries(uEyeCamera ueye_api ${catkin_LIBRARIES})
add_dependencies(uEyeCamera ${PROJECT_NAME}_gencfg ${catkin_EXPORTED_TARGETS})

add_library(uEyeCameraNodelet src/nodelet.cpp src/uEyeConfigDbg.cpp)
target_link_libraries(uEyeCameraNodelet uEyeCamera ${catkin_LIBRARIES})
add_dependencies(uEyeCameraNodelet ${catkin_EXPORTED_TARGETS})

add_executable(ueye_camera_node src/node.cpp)
target_link_libraries(ueye_camera_node uEyeCamera ${catkin_LIBRARIES})
set_target_properties(ueye_camera_node
                      PROPERTIES OUTPUT_NAME camera_node PREFIX "")
add_dependencies(ueye_camera_node ${catkin_EXPORTED_TARGETS})

add_executable(ueye_driver src/main.cpp src/uEyeCamera.cpp src/uEyeConfigDbg.cpp)
target_link_libraries(ueye_driver ${catkin_LIBRARIES} ${OpenCV_LIBRARIES} ${YAML_CPP_LIBRARIES} ueye_api)
set_target_properties(ueye_driver
                      PROPERTIES OUTPUT_NAME driver PREFIX "")

add_executable(ueye_list_cameras src/list_cameras.cpp src/uEyeCamera.cpp)
target_link_libraries(ueye_list_cameras ${catkin_LIBRARIES} ${OpenCV_LIBRARIES} ueye_api)
set_target_properties(ueye_list_cameras
                      PROPERTIES OUTPUT_NAME list_cameras PREFIX "")

