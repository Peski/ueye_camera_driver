/*
This code was developed by the National Robotics Engineering Center (NREC), part of the Robotics Institute at Carnegie Mellon University.
Its development was funded by DARPA under the LS3 program and submitted for public release on June 7th, 2012.
Release was granted on August, 21st 2012 with Distribution Statement "A" (Approved for Public Release, Distribution Unlimited).

This software is released under a BSD license:

Copyright (c) 2012, Carnegie Mellon University. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
Neither the name of the Carnegie Mellon University nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/



/*-*-C++-*-*/
/**
   @file PointGreyCamera.cpp
   @author Chad Rockey
   @date July 11, 2011
   @brief Interface to Point Grey cameras

   @attention Copyright (C) 2011
   @attention National Robotics Engineering Center
   @attention Carnegie Mellon University
*/

#include "ueye_camera_driver/uEyeCamera.h"

#include <algorithm>
#include <iterator>
#include <stdexcept>
#include <utility>
#include <vector>

#include <ros/ros.h>

#include <sensor_msgs/image_encodings.h> // ROS header for the different supported image encoding types
#include <sensor_msgs/fill_image.h>

uEyeCamera::uEyeCamera()//:
  //busMgr_(), cam_()
{
  id_ = 0;
  cam_ = 0;

  captureRunning_ = false;

  img_mem_ = nullptr;
  mem_id_ = 0;

  encoding_ = "";
  width_ = 0;
  height_ = 0;
  step_ = 0;
}

uEyeCamera::~uEyeCamera()
{
  disconnect();
}

bool uEyeCamera::setNewConfiguration(ueye_camera_driver::uEyeConfig& config, const uint32_t& level)
{
  if (cam_ == 0) {
    uEyeCamera::connect();
  }

  // Activate mutex to prevent us from grabbing images during this time
  //boost::mutex::scoped_lock scopedLock(mutex_);

  // return true if we can set values as desired.
  bool retVal = true;

  if (captureRunning_ && level != uEyeCamera::LEVEL_RECONFIGURE_RUNNING) {
    // TODO Stop sensor
    throw std::runtime_error("uEyeCamera::setNewConfiguration Dynamic reconfigure not supported yet!");
  }

  INT error;

  // Color mode
  INT colorMode;
  retVal &= uEyeCamera::getColorModeFromString(config.color_mode, colorMode);
  error = is_SetColorMode(cam_, colorMode);
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set color mode", error);
  retVal &= (is_SetColorMode(cam_, IS_GET_COLOR_MODE) == colorMode);

  // Allocate image memory
  INT bits;
  if (colorMode == IS_CM_MONO8) {
    bits = 8;
  } else {
    throw std::runtime_error("uEyeCamera::setNewConfiguration Unsupported color mode!");
  }

  SENSORINFO sensor_info;
  error = is_GetSensorInfo (cam_, &sensor_info);
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to get sensor information", error);

  INT width = sensor_info.nMaxWidth, height = sensor_info.nMaxHeight;
  error = is_AllocImageMem(cam_, width, height, bits, &img_mem_, &mem_id_);
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to allocate image memory", error);

  error = is_SetImageMem(cam_, img_mem_, mem_id_);
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set active image memory", error);

  encoding_ = config.color_mode;
  width_ = width;
  height_ = height;
  step_ = width * ((bits + 7) / 8);

  // TODO Set pixel clock to closest supported
  // Pixel clock
/*
  UINT nPixelClocks = 0;
  error = is_PixelClock(cam_, IS_PIXELCLOCK_CMD_GET_NUMBER, reinterpret_cast<void*>(&nPixelClocks), sizeof(nPixelClocks));
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to get number of supported pixel clocks", error);

  std::vector<UINT> pixelClocks(nPixelClocks, 0);
  error = is_PixelClock(cam_, IS_PIXELCLOCK_CMD_GET_LIST, reinterpret_cast<void*>(pixelClocks.data()), nPixelClocks * sizeof(UINT));
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to get pixel clock list", error);

  std::sort(pixelClocks.begin(), pixelClocks.end());
  std::vector<UINT>::const_iterator c_it = std::lower_bound(pixelClocks.cbegin(), pixelClocks.cend(), config.pixel_clock);
  //

  UINT nPixelClock;
  //
  retVal &= (nPixelClock == config.pixel_clock);
  config.pixel_clock = nPixelClock;
  error = is_PixelClock(cam_, IS_PIXELCLOCK_CMD_SET, reinterpret_cast<void*>(nPixelClock), sizeof(nPixelClock));
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set pixel clock", error);

  error = is_PixelClock(cam_, IS_PIXELCLOCK_CMD_GET, reinterpret_cast<void*>(&nPixelClock), sizeof(nPixelClock));
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to get pixel clock", error);
  retVal &= (nPixelClock == config.pixel_clock);
*/
  error = is_PixelClock(cam_, IS_PIXELCLOCK_CMD_SET, reinterpret_cast<void*>(&config.pixel_clock), sizeof(config.pixel_clock));
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set pixel clock", error);

  // Frame rate
  // TODO Should return if desired FPS could be set?
  // TODO Only if trigger is not enabled
  double fps;
  is_SetFrameRate(cam_, config.frame_rate, &fps);
  //uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set frame rate", error);
  //retVal &= (std::abs() < std::numeric_limits);
  config.frame_rate = fps;

  // TODO Auto features with is_GetAutoInfo

  // TODO Auto exposure
  // When the auto exposure shutter function is enabled, you cannot modify the pixel clock frequency
/*
  double enable_autosetting;
  double dummy;
  enable_autosetting = config.auto_exposure ? 1.0:0.0;
  error = is_SetAutoParameter(cam_, IS_SET_ENABLE_AUTO_SHUTTER, &enable_autosetting, &dummy);
  if (error == IS_NOT_SUPPORTED) {
    enable_autosetting = 0.0;
    error = is_SetAutoParameter(cam_, IS_SET_ENABLE_AUTO_SHUTTER, &enable_autosetting, &dummy);
    uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set auto exposure (2)", error);
    retVal &= (config.auto_exposure == false);
    config.auto_exposure = false;
  } else {
    uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set auto exposure (1)", error);
  }
*/

  // Auto gain
/*
  enable_autosetting = config.auto_exposure ? 1.0:0.0;
  error = is_SetAutoParameter(cam_, IS_SET_ENABLE_AUTO_SHUTTER, &enable_autosetting, &dummy);
  if (error == IS_NOT_SUPPORTED) {
    enable_autosetting = 0.0;
    error = is_SetAutoParameter(cam_, IS_SET_ENABLE_AUTO_SHUTTER, &enable_autosetting, &dummy);
    uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set auto exposure (2)", error);
    retVal &= (config.auto_exposure == false);
    config.auto_exposure = false;
  } else {
    uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set auto exposure (1)", error);
  }
*/

  // Exposure time
  //if (!config.auto_exposure) {
    is_Exposure(cam_, IS_EXPOSURE_CMD_SET_EXPOSURE, &config.exposure_time, sizeof(config.exposure_time));
    //uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set exposure time", error);
    //is_Exposure(cam_, IS_EXPOSURE_CMD_SET_EXPOSURE, &config.exposure_time, sizeof(config.exposure_time));
  //}

  // Hardware gamma
  bool gamma_supported = (is_SetHardwareGamma(cam_, IS_GET_HW_SUPPORTED_GAMMA) == IS_SET_HW_GAMMA_ON);
  error = is_SetHardwareGamma(cam_, (gamma_supported && config.hardware_gamma) ? IS_SET_HW_GAMMA_ON:IS_SET_HW_GAMMA_OFF);
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set hardware gamma", error);
  bool hardware_gamma = is_SetHardwareGamma(cam_, IS_GET_HW_GAMMA) == IS_SET_HW_GAMMA_ON;
  retVal &= (hardware_gamma == config.hardware_gamma);
  config.hardware_gamma = hardware_gamma;

  // Gain boost
  bool gainboost_supported = (is_SetGainBoost(cam_, IS_GET_SUPPORTED_GAINBOOST) == IS_SET_GAINBOOST_ON);
  error = is_SetGainBoost(cam_, (gainboost_supported && config.gain_boost) ? IS_SET_GAINBOOST_ON:IS_SET_GAINBOOST_OFF);
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set gain boost", error);
  bool gain_boost = is_SetGainBoost(cam_, IS_GET_GAINBOOST) == IS_SET_GAINBOOST_ON;
  retVal &= (gain_boost == config.gain_boost);
  config.gain_boost = gain_boost;

  INT nRet;

  // Black level
  INT nBlacklevelCaps;
  nRet = is_Blacklevel(cam_, IS_BLACKLEVEL_CMD_GET_CAPS, (void*)&nBlacklevelCaps, sizeof(nBlacklevelCaps));
  if (nRet == IS_SUCCESS) {
    if (nBlacklevelCaps & IS_BLACKLEVEL_CAP_SET_AUTO_BLACKLEVEL) {
      // Set auto mode
      INT nMode = IS_AUTO_BLACKLEVEL_OFF;
      nRet = is_Blacklevel(cam_, IS_BLACKLEVEL_CMD_SET_MODE, (void*)&nMode , sizeof(nMode));
    }

    if (nBlacklevelCaps & IS_BLACKLEVEL_CAP_SET_OFFSET) {
      IS_RANGE_S32 nRange;
      nRet = is_Blacklevel(cam_, IS_BLACKLEVEL_CMD_GET_OFFSET_RANGE, (void*)&nRange, sizeof(nRange));
      INT nOffsetMin = nRange.s32Min;
      INT nOffsetMax = nRange.s32Max;
      INT nOffsetInc = nRange.s32Inc;

      nRet = is_Blacklevel(cam_, IS_BLACKLEVEL_CMD_SET_OFFSET, (void*)&nOffsetMin, sizeof(nOffsetMin));
    }
  }

  // Hot pixel correction
  nRet = is_HotPixel(cam_, IS_HOTPIXEL_DISABLE_CORRECTION, NULL, NULL);
  nRet = is_HotPixel(cam_, IS_HOTPIXEL_DISABLE_SENSOR_CORRECTION, NULL, NULL);

  // Adaptive hot pixel correction
  INT nEnable = IS_HOTPIXEL_ADAPTIVE_CORRECTION_DISABLE;
  nRet = is_HotPixel(cam_, IS_HOTPIXEL_ADAPTIVE_CORRECTION_SET_ENABLE, (void*)&nEnable, sizeof(nEnable));

  INT trigger_delay = 0;
  retVal &= uEyeCamera::setExternalTrigger(config.enable_trigger, config.trigger_mode, trigger_delay);

  //TODO Don't crash if not supported
/*
  double pval1, pval2;

  pval1 = 1.0;
  error = is_SetAutoParameter(cam_, IS_SET_ENABLE_AUTO_SENSOR_SHUTTER, &pval1, NULL);
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set auto shutter", error);

  pval1 = 10.0;
  is_SetAutoParameter(cam_, IS_SET_AUTO_SHUTTER_MAX, &pval1, NULL);
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set auto shutter max", error);

  pval1 = 100.0;
  is_SetAutoParameter(cam_, IS_SET_AUTO_SPEED, &pval1, NULL);
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set auto speed", error);

  pval1 = 0.0;
  is_SetAutoParameter(cam_, IS_SET_AUTO_SKIPFRAMES, &pval1, NULL);
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set auto skip", error);

  pval1 = AS_PM_SENS_CENTER_SPOT;
  is_SetAutoParameter(cam_, IS_SET_SENS_AUTO_SHUTTER_PHOTOM, &pval1, NULL);
  uEyeCamera::handleError("uEyeCamera::setNewConfiguration Failed to set auto shutter mode", error);
*/

  exposure_times_ = uEyeCamera::getExposureTimes();

  return retVal;
}

void uEyeCamera::connect()
{
  cam_ = id_;

  INT error;
  error = is_InitCamera(&cam_, NULL);
  uEyeCamera::handleError("uEyeCamera::connect Failed to connect to camera", error);
}

void uEyeCamera::disconnect()
{
  if (cam_ != 0) {
    INT error;

    if (img_mem_) {
      INT error = is_FreeImageMem(cam_, img_mem_, mem_id_);
      uEyeCamera::handleError("uEyeCamera::disconnect Failed to free image memory", error);

      img_mem_ = nullptr;
      mem_id_ = 0;
    }

    error = is_ExitCamera(cam_);
    uEyeCamera::handleError("uEyeCamera::disconnect Failed to release camera", error);
    cam_ = 0;
  }
}

void uEyeCamera::start()
{
  if(cam_ && !captureRunning_) {
    if (!img_mem_)
      throw std::runtime_error("uEyeCamera::start Camera configuration not set! Use setNewConfiguration before calling start()");

    // Start capturing images
    INT error;

    error = is_CaptureVideo(cam_, IS_DONT_WAIT);
    uEyeCamera::handleError("uEyeCamera::start Failed to start capture", error);

    error = is_EnableEvent(cam_, IS_SET_EVENT_FRAME);
    uEyeCamera::handleError("uEyeCamera::start Failed to enable capture event", error);

    captureRunning_ = true;
  }
}

bool uEyeCamera::stop()
{
  if(cam_ && captureRunning_) {
    // Stop capturing images
    captureRunning_ = false;

    INT error;

    error = is_DisableEvent(cam_, IS_SET_EVENT_FRAME);
    uEyeCamera::handleError("uEyeCamera::stop Failed to disable capture event", error);

    error = is_StopLiveVideo(cam_, IS_FORCE_VIDEO_STOP);
    uEyeCamera::handleError("uEyeCamera::stop Failed to stop capture", error);

    return true;
  }

  return false;
}

void uEyeCamera::grabImage(sensor_msgs::Image& image)
{
  //boost::mutex::scoped_lock scopedLock(mutex_);
  if(cam_ && captureRunning_) {
    INT error;

    // TODO Timeout
    //while (error == IS_TIMED_OUT) is_WaitEvent(cam_, IS_SET_EVENT_FRAME, 10); // 10 ms
    //if (error != IS_SUCCESS) uEyeCamera::handleError("uEyeCamera::grabImage Failed to wait for image", error);

    error = is_WaitEvent(cam_, IS_SET_EVENT_FRAME, INFINITE);
    uEyeCamera::handleError("uEyeCamera::grabImage Failed to acquire image", error);

    ros::Time stamp = ros::Time::now();

/*
    double current_exposure;
    error = is_Exposure(cam_, IS_EXPOSURE_CMD_GET_EXPOSURE, &current_exposure, sizeof(current_exposure));
    uEyeCamera::handleError("uEyeCamera::grabImage Failed to get exposure time", error);
*/

    fillImage(image, encoding_, height_, width_, step_, img_mem_);

    // Set the header timestamp
    image.header.stamp = stamp;

    // Set the frame_id to exposure time index
/*
    std::vector<double>::const_iterator cit = std::lower_bound(exposure_times_.cbegin(), exposure_times_.cend(), current_exposure);
    if (cit != exposure_times_.cbegin()) std::advance(cit, -1);
    image.header.frame_id = std::to_string(std::distance(exposure_times_.cbegin(), cit));
*/
  } else if (cam_) {
    throw CameraNotRunningException("uEyeCamera::grabImage: Camera is currently not running.  Please start the capture.");
  } else {
    throw std::runtime_error("uEyeCamera::grabImage not connected!");
  }
}

void uEyeCamera::setDesiredCamera(const std::uint32_t &id)
{
  id_ = id;
}

std::vector<double> uEyeCamera::getExposureTimes()
{
  if(cam_) {
    INT error;

    double min;
    error = is_Exposure(cam_, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MIN, &min, sizeof(min));
    uEyeCamera::handleError("uEyeCamera::getExposureTimes Failed to get range min", error);

    double max;
    error = is_Exposure(cam_, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MAX, &max, sizeof(max));
    uEyeCamera::handleError("uEyeCamera::getExposureTimes Failed to get range max", error);

    double inc;
    error = is_Exposure(cam_, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_INC, &inc, sizeof(inc));
    uEyeCamera::handleError("uEyeCamera::getExposureTimes Failed to get range inc", error);

    std::size_t n = static_cast<std::size_t>((max - min) / inc);
    std::vector<double> exposure_times(n, -1.0);

    for (std::size_t i = 0; i < n; ++i)
      exposure_times[i] = i*inc + min;

    return std::move(exposure_times);
  } else {
    throw std::runtime_error("uEyeCamera::getExposureTimes not connected!");
  }
  
}

bool uEyeCamera::getColorModeFromString(std::string &cmode, INT &cmode_out)
{
  // return true if we can set values as desired.
  bool retVal = true;

  // TODO Check if camera supports the desired color mode (for now only mono8 supported)

  if (cmode.compare("mono8") == 0) {
    cmode_out = IS_CM_MONO8;
  } else {
    cmode = "mono8";
    cmode_out = IS_CM_MONO8;
    retVal &= false;
  }

  return retVal;
}

bool uEyeCamera::setExternalTrigger(bool &enable, std::string& mode, INT& delay)
{
  // return true if we can set values as desired.
  bool retVal = true;

  INT error;

  INT nTriggerMode = IS_SET_TRIGGER_OFF;

  // Get trigger mode
  if (enable){
    if (mode.compare("hi_lo") == 0) {
      nTriggerMode = IS_SET_TRIGGER_HI_LO;
    } else if (mode.compare("lo_hi") == 0) {
      nTriggerMode = IS_SET_TRIGGER_LO_HI;
    } else {
      mode = "hi_lo";
      enable = false;
    }
  }

  error = is_SetExternalTrigger(cam_, nTriggerMode);
  uEyeCamera::handleError("uEyeCamera::setExternalTrigger Failed to set external trigger", error);
  nTriggerMode = is_SetExternalTrigger(cam_, IS_GET_EXTERNALTRIGGER);

  std::string mode_set = mode;
  bool enable_set = enable;
  if (nTriggerMode == IS_SET_TRIGGER_HI_LO) {
    enable_set = true;
    mode_set = "hi_lo";
  } else if (nTriggerMode == IS_SET_TRIGGER_LO_HI) {
    enable_set = true;
    mode_set = "lo_hi";
  } else {
    enable_set = false;
  }
  retVal &= (enable_set == enable);
  enable = enable_set;

  if (enable) retVal &= (mode_set.compare(mode) == 0);
  mode = mode_set;

  error = is_SetTriggerDelay(cam_, delay);
  uEyeCamera::handleError("uEyeCamera::setExternalTrigger Failed to set trigger delay", error);
  INT delay_set = is_SetTriggerDelay(cam_, IS_GET_TRIGGER_DELAY);
  retVal &= (delay_set == delay);
  delay = delay_set;

  return retVal;
}

void uEyeCamera::handleError(const std::string& prefix, const INT error)
{
  // TODO
  //if(error == PGRERROR_TIMEOUT)
  //{
  //  throw CameraTimeoutException("PointGreyCamera: Failed to retrieve buffer within timeout.");
  //}
  //else if(error == PGRERROR_IMAGE_CONSISTENCY_ERROR)
  //{
  //  throw CameraImageConsistencyError("PointGreyCamera: Image consistency error.");
  //}
  //else if(error != PGRERROR_OK)     // If there is actually an error (PGRERROR_OK means the function worked as intended...)
  //{
  //  std::string start(" | FlyCapture2::ErrorType ");
  //  std::stringstream out;
  //  out << error.GetType();
  //  std::string desc(error.GetDescription());
  //  throw std::runtime_error(prefix + start + out.str() + " " + desc);
  //}
  if (error != IS_SUCCESS) { // If there is actually an error (IS_SUCCESS means the function worked as intended...)
    throw std::runtime_error(prefix + " (error code: " + std::to_string(error) + ")");
  }
}

  /*!
  * \brief Returns the IDs of the currently available attached cameras
  * 
  * Provides a way to query free, i.e. available, currently not in use, cameras connected to the system. The provided camera
  * ids can be used to connect to a camera calling setDesiredCamera(id) first and then connect() functions.
  * \return Returns a vector containing the ID of all unused cameras
  */
std::vector<uint32_t> uEyeCamera::getAttachedCameras()
{
  std::vector<uint32_t> cameras;

  INT num_cameras = 0;
  INT error = IS_SUCCESS;

  error = is_GetNumberOfCameras(&num_cameras);
  uEyeCamera::handleError("uEyeCamera::getAttachedCameras: Could not get number of cameras", error);

  UEYE_CAMERA_LIST* pucl;
  pucl = (UEYE_CAMERA_LIST*) new BYTE [sizeof (DWORD) + num_cameras * sizeof (UEYE_CAMERA_INFO)];
  pucl->dwCount = num_cameras;

  error = is_GetCameraList(pucl);
  uEyeCamera::handleError("uEyeCamera::getAttachedCameras: Could not get camera list", error);

  for (int i = 0; i < num_cameras; ++i) {
    uint32_t this_id;
    this_id = pucl->uci[i].dwCameraID;
    cameras.push_back(this_id);
  }

  return cameras;
}

