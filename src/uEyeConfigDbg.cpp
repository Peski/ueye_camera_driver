
#include "ueye_camera_driver/uEyeConfigDbg.h"

namespace ueye_camera_driver
{

/*
uEyeConfig::uEyeConfig()
  : color_mode("mono8"), pixel_clock(15), frame_rate(20.0),
    auto_exposure(true), exposure_time(1.0), auto_gain(true),
    hardware_gain(0), gain_boost(false), hardware_gamma(true),
    enable_trigger(true), trigger_mode("hi_lo")
{
}
*/

uEyeConfig::uEyeConfig()
  : color_mode("mono8"), pixel_clock(24), frame_rate(10.0),
    auto_exposure(false), exposure_time(5.0), auto_gain(false),
    hardware_gain(0), gain_boost(false), hardware_gamma(false),
    enable_trigger(false), trigger_mode("hi_lo")
{
}

uEyeConfig::uEyeConfig(const std::string& config_file)
  : uEyeConfig()
{
  load(config_file);
}

uEyeConfig::~uEyeConfig()
{
}

void uEyeConfig::load(const std::string& config_file)
{
  // TODO yaml
}

}
