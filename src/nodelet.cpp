/*
This code was developed by the National Robotics Engineering Center (NREC), part of the Robotics Institute at Carnegie Mellon University.
Its development was funded by DARPA under the LS3 program and submitted for public release on June 7th, 2012.
Release was granted on August, 21st 2012 with Distribution Statement "A" (Approved for Public Release, Distribution Unlimited).

This software is released under a BSD license:

Copyright (c) 2012, Carnegie Mellon University. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
Neither the name of the Carnegie Mellon University nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/



/**
   @file stereo_nodelet.cpp
   @author Chad Rockey
   @date March 9, 2011
   @brief ROS nodelet for the Point Grey Stereo Cameras

   @attention Copyright (C) 2012
   @attention National Robotics Engineering Center
   @attention Carnegie Mellon University
*/

// ROS and associated nodelet interface and PLUGINLIB declaration header
#include <ros/ros.h>
#include <pluginlib/class_list_macros.h>
#include <nodelet/nodelet.h>

#include "ueye_camera_driver/uEyeCamera.h" // The actual standalone uEye library

#include <image_transport/image_transport.h> // ROS library that allows sending compressed images
#include <sensor_msgs/Image.h> // ROS message header for Image
//#include <camera_info_manager/camera_info_manager.h> // ROS library that publishes CameraInfo topics
//#include <sensor_msgs/CameraInfo.h> // ROS message header for CameraInfo
//#include <std_msgs/Float64.h>

//#include <wfov_camera_msgs/WFOVImage.h>
//#include <image_exposure_msgs/ExposureSequence.h> // Message type for configuring gain and white balance.

//#include <diagnostic_updater/diagnostic_updater.h> // Headers for publishing diagnostic messages.
//#include <diagnostic_updater/publisher.h>

#include <boost/thread.hpp> // Needed for the nodelet to launch the reading thread.

//#include <dynamic_reconfigure/server.h> // Needed for the dynamic_reconfigure gui service to run

namespace ueye_camera_driver
{

class uEyeCameraNodelet: public nodelet::Nodelet
{
public:
  uEyeCameraNodelet() {}

  ~uEyeCameraNodelet()
  {
    pubThread_->interrupt();
    pubThread_->join();
    cleanUp();
  }

private:
  /*!
  * \brief Function that allows reconfiguration of the camera.
  *
  * This function serves as a callback for the dynamic reconfigure service.  It simply passes the configuration object to the driver to allow the camera to reconfigure.
  * \param config  camera_library::CameraConfig object passed by reference.  Values will be changed to those the driver is currently using.
  * \param level driver_base reconfiguration level.  See driver_base/SensorLevels.h for more information.
  */
/*
  void paramCallback(pointgrey_camera_driver::PointGreyConfig &config, uint32_t level)
  {

    // Stereo is only active in this mode (16 bits, 8 for each image)
    config.video_mode = "format7_mode3";

    try
    {
      NODELET_DEBUG("Dynamic reconfigure callback with level: %d", level);
      pg_.setNewConfiguration(config, level);

      // Store needed parameters for the metadata message
      gain_ = config.gain;
      wb_blue_ = config.white_balance_blue;
      wb_red_ = config.white_balance_red;


      // Store CameraInfo binning information
      if(config.video_mode == "640x480_mono8" || config.video_mode == "format7_mode1")
      {
        binning_x_ = 2;
        binning_y_ = 2;
      }
      else if(config.video_mode == "format7_mode2")
      {
        binning_x_ = 0;
        binning_y_ = 2;
      }
      else
      {
        binning_x_ = 0;
        binning_y_ = 0;
      }

      // Store CameraInfo RegionOfInterest information
      if(config.video_mode == "format7_mode0" || config.video_mode == "format7_mode1" || config.video_mode == "format7_mode2")
      {
        roi_x_offset_ = config.format7_x_offset;
        roi_y_offset_ = config.format7_y_offset;
        roi_width_ = config.format7_roi_width;
        roi_height_ = config.format7_roi_height;
        do_rectify_ = true; // Set to true if an ROI is used.
      }
      else
      {
        // Zeros mean the full resolution was captured.
        roi_x_offset_ = 0;
        roi_y_offset_ = 0;
        roi_height_ = 0;
        roi_width_ = 0;
        do_rectify_ = false; // Set to false if the whole image is captured.
      }
    }
    catch(std::runtime_error& e)
    {
      NODELET_ERROR("Reconfigure Callback failed with error: %s", e.what());
    }
  }
*/

  /*!
  * \brief Serves as a psuedo constructor for nodelets.
  *
  * This function needs to do the MINIMUM amount of work to get the nodelet running.  Nodelets should not call blocking functions here for a significant period of time.
  */
  void onInit()
  {
    // Get nodeHandles
    ros::NodeHandle &nh = getMTNodeHandle();
    ros::NodeHandle &pnh = getMTPrivateNodeHandle();
    std::string cam;
    pnh.param<std::string>("camera_namespace", cam, "");
    ros::NodeHandle cnh(getMTNodeHandle(), cam);

    // Get a serial number through ros
    int cameraIdParam;
    pnh.param<int>("camera_id", cameraIdParam, 0);
    uint32_t camera_id = (uint32_t)cameraIdParam;
    // Get the location of our camera config yamls
    //std::string camera_info_url;
    //pnh.param<std::string>("camera_info_url", camera_info_url, "");
    // Get the desired frame_id, set to 'camera' if not found
    //pnh.param<std::string>("frame_id", frame_id_, "camera");

    // Try connecting to the camera
    volatile bool connected = false;
    while(!connected && ros::ok())
    {
      try
      {
        NODELET_INFO("Connecting to camera %u", camera_id);
        ue_.setDesiredCamera(camera_id);
        NODELET_DEBUG("Actually connecting to camera.");
        ue_.connect();
        connected = true;
      }
      catch(std::runtime_error& e)
      {
        NODELET_ERROR("%s", e.what());
        ros::Duration(1.0).sleep(); // sleep for one second each time
      }
    }

    // Start up the dynamic_reconfigure service, note that this needs to stick around after this function ends
    //srv_ = boost::make_shared <dynamic_reconfigure::Server<pointgrey_camera_driver::PointGreyConfig> > (pnh);
    //dynamic_reconfigure::Server<pointgrey_camera_driver::PointGreyConfig>::CallbackType f =  boost::bind(&pointgrey_camera_driver::PointGreyStereoCameraNodelet::paramCallback, this, _1, _2);
    //srv_->setCallback(f);

    // Load config
    // TODO Use dynamic reconfigure instead
    ueye_camera_driver::uEyeConfig config; // Using default config
    ue_.setNewConfiguration(config, uEyeCamera::LEVEL_RECONFIGURE_STOP);

    // Start the camera info manager and attempt to load any configurations
    //std::stringstream cinfo_name;
    //cinfo_name << camera_id;
    //cinfo_.reset(new camera_info_manager::CameraInfoManager(cnh, cinfo_name.str(), camera_info_url));
    /*if (cinfo_->validateURL(camera_info_url)){ // Load CameraInfo (if any) from the URL.  Will ROS_ERROR if bad.
      cinfo_->loadCameraInfo(camera_info_url);
    }*/
    //ci_.reset(new sensor_msgs::CameraInfo(cinfo_->getCameraInfo()));
    //ci_->header.frame_id = frame_id_;

    // Publish topics using ImageTransport through camera_info_manager (gives cool things like compression)
    it_.reset(new image_transport::ImageTransport(cnh));
    it_pub_ = it_->advertise("image_raw", 5);

    volatile bool started = false;
    while(!started)
    {
      try
      {
        NODELET_DEBUG("Starting camera capture.");
        ue_.start();
        started = true;
        // Start the thread to loop through and publish messages
        pubThread_ = boost::shared_ptr< boost::thread > (new boost::thread(boost::bind(&ueye_camera_driver::uEyeCameraNodelet::devicePoll, this)));
      }
      catch(std::runtime_error& e)
      {
        NODELET_ERROR("%s", e.what());
        ros::Duration(1.0).sleep(); // sleep for one second each time
      }
    }
  }

  /*!
  * \brief Cleans up the memory and disconnects the camera.
  *
  * This function is called from the deconstructor since ue_.stop() and ue_.disconnect() could throw exceptions.
  */
  void cleanUp()
  {
    try
    {
      NODELET_DEBUG("Stopping camera capture.");
      ue_.stop();
      NODELET_DEBUG("Disconnecting from camera.");
      ue_.disconnect();
    }
    catch(std::runtime_error& e)
    {
      NODELET_ERROR("%s", e.what());
    }
  }

  /*!
  * \brief Function for the boost::thread to grabImage and publish it.
  *
  * This function continues until the thread is interupted.  Responsible for getting sensor_msgs::Image and publishing them.
  */
  void devicePoll()
  {
    while(!boost::this_thread::interruption_requested())   // Block until we need to stop this thread.
    {
      try
      {
        sensor_msgs::ImagePtr image(new sensor_msgs::Image);
        ue_.grabImage(*image);

        ros::Time time = ros::Time::now();
        image->header.stamp = time;

        it_pub_.publish(image);
      }
      catch(CameraTimeoutException& e)
      {
        NODELET_WARN("%s", e.what());
      }
      catch(std::runtime_error& e)
      {
        NODELET_ERROR("%s", e.what());
        try
        {
          // Something terrible has happened, so let's just disconnect and reconnect to see if we can recover.
          ue_.disconnect();
          ros::Duration(1.0).sleep(); // sleep for one second each time
          ue_.connect();
          ue_.start();
        }
        catch(std::runtime_error& e2)
        {
          NODELET_ERROR("%s", e2.what());
        }
      }
    }
  }

  //boost::shared_ptr<dynamic_reconfigure::Server<pointgrey_camera_driver::PointGreyConfig> > srv_; ///< Needed to initialize and keep the dynamic_reconfigure::Server in scope.
  boost::shared_ptr<image_transport::ImageTransport> it_; ///< Needed to initialize and keep the ImageTransport in scope.
  //boost::shared_ptr<camera_info_manager::CameraInfoManager> cinfo_; ///< Needed to initialize and keep the CameraInfoManager in scope.
  image_transport::Publisher it_pub_; ///< Image ROS publisher
  //ros::Publisher temp_pub_; ///< Publisher for current camera temperature @todo Put this in diagnostics instead.
  //ros::Subscriber sub_; ///< Subscriber for gain and white balance changes.

  uEyeCamera ue_; ///< Instance of the uEye library, used to interface with the hardware.
  //sensor_msgs::CameraInfoPtr ci_; ///< Camera Info message.
  //std::string frame_id_; ///< Frame id for the camera messages, defaults to 'camera'
  boost::shared_ptr<boost::thread> pubThread_; ///< The thread that reads and publishes the images.
};

PLUGINLIB_EXPORT_CLASS(ueye_camera_driver::uEyeCameraNodelet, nodelet::Nodelet)  // Needed for Nodelet declaration
}
