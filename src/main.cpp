

// STL
#include <cstddef>
#include <cstring>
#include <exception>
#include <iostream>

// OpenCV
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <cv_bridge/cv_bridge.h>

#include "ueye_camera_driver/uEyeCamera.h"

#include "macros.h"

/*
sensor_msgs::ImagePtr image(new sensor_msgs::Image);
ueye_.grabStereoImage(*image, frame_id_);

ros::Time time = ros::Time::now();
image->header.stamp = time;
ci_->header.stamp = time;

it_pub_.publish(image, ci_);
*/

int main() {

  std::vector<std::uint32_t> camera_ids = uEyeCamera::getAttachedCameras();
  if (!camera_ids.empty()) {
    uEyeCamera camera;
    camera.setDesiredCamera(camera_ids.front());

    ueye_camera_driver::uEyeConfig config;
    
    camera.connect();
    camera.setNewConfiguration(config, uEyeCamera::LEVEL_RECONFIGURE_STOP);
    camera.start();

    //

    camera.stop();
    camera.disconnect();
  }  

  return 0;
}

/*
#include  "Camera.h"
ueye::Camera cam;

void publishImage(const char *frame, size_t size) {
    cv::Mat mat(cam.getHeight(), cam.getWidth(), CV_8UC1, (uchar*)(frame));
    cv::imshow("Frame", mat);
    cv::waitKey(1);
}

int main() {

    int nCameras = cam.getNumberOfCameras();
    RUNTIME_ASSERT(nCameras >= 1);

    //cv::namedWindow("Frame", cv::WINDOW_AUTOSIZE);

    int id = 0;
    RUNTIME_ASSERT(cam.openCameraCamId(id));
    std::cout << cam.getCameraName() << " " << cam.getCameraSerialNo() << std::endl;
    //cam.setTriggerMode(ueye::TriggerMode::TRIGGER_HI_LO);

    cam.startVideoCapture(publishImage);
    std::getchar();
    cam.stopVideoCapture();

    return 0;
}
*/

/*
int main() {
    
    std::cout << "uEye drvier" << std::endl;

    INT nNumCam = 0;
    RUNTIME_ASSERT(is_GetNumberOfCameras(&nNumCam) == IS_SUCCESS);
    if (nNumCam >= 1) {
        std::cout << "Number of Cameras: " << nNumCam << std::endl;

        // Create new list with suitable size
        UEYE_CAMERA_LIST* pucl;
        pucl = (UEYE_CAMERA_LIST*) new BYTE [sizeof (DWORD) + nNumCam * sizeof (UEYE_CAMERA_INFO)];
        pucl->dwCount = nNumCam;

        //Retrieve camera info
        if (is_GetCameraList(pucl) == IS_SUCCESS) {
            for (int iCamera = 0; iCamera < (int)pucl->dwCount; iCamera++) {
                //Test output of camera info on the screen
                std::cout << "Camera " << iCamera << " Id: " << pucl->uci[iCamera].SerNo << std::endl;
                //printf("Camera %i Id: %d", iCamera, pucl->uci[iCamera].dwCameraID);
            }
          }

          delete [] pucl;
    }

    //Open camera with ID 1
    HIDS hCam = 1;
    RUNTIME_ASSERT(is_InitCamera(&hCam, NULL) == IS_SUCCESS);

    std::exception_ptr eptr;
    char* pcImageMem = nullptr;
    INT id;

    try {
        is_SetErrorReport(hCam, IS_ENABLE_ERR_REP); // For debug only

        SENSORINFO Info;
        RUNTIME_ASSERT(is_GetSensorInfo(hCam, &Info) == IS_SUCCESS);

        std::cout << "Color Mode: " << is_SetColorMode(hCam, IS_GET_COLOR_MODE) << std::endl;

        //std::cout << "Color Mode: " << static_cast<unsigned>(Info.nColorMode) << std::endl;
        std::cout << "Max Width: " << Info.nMaxWidth << std::endl;
        std::cout << "Max Height: " << Info.nMaxHeight << std::endl;
        std::cout << "Global Shutter?: " << Info.bGlobShutter << std::endl;

        UINT nNumFormats = 0;
        RUNTIME_ASSERT(is_ImageFormat(hCam, IMGFRMT_CMD_GET_NUM_ENTRIES, &nNumFormats, 4) == IS_SUCCESS);

        UINT nBytes = sizeof(IMAGE_FORMAT_LIST) + sizeof(nNumFormats - 1) * sizeof(IMAGE_FORMAT_INFO);
        IMAGE_FORMAT_LIST* pImageFormatList;
        pImageFormatList = (IMAGE_FORMAT_LIST*) new BYTE [nBytes];

        pImageFormatList->nSizeOfListEntry = sizeof(IMAGE_FORMAT_INFO);
        pImageFormatList->nNumListElements = nNumFormats;
        RUNTIME_ASSERT(is_ImageFormat(hCam, IMGFRMT_CMD_GET_LIST, pImageFormatList, nBytes) == IS_SUCCESS);

        IMAGE_FORMAT_INFO formatInfo = pImageFormatList->FormatInfo[0];
        std::cout << "FormatID: " << formatInfo.nFormatID << std::endl;
        std::cout << "Width: " << formatInfo.nWidth << std::endl;
        std::cout << "Height: " << formatInfo.nHeight << std::endl;

//        for (int i = 0; i < nNumFormats; ++i) {
//            formatInfo = pImageFormatList->FormatInfo[i];

//            std::cout << "Width: " << formatInfo.nWidth << std::endl;
//            std::cout << "Height: " << formatInfo.nHeight << std::endl;
//            std::cout << "FormatID: " << formatInfo.nFormatID << std::endl;
//        }

        RUNTIME_ASSERT(is_ImageFormat(hCam, IMGFRMT_CMD_SET_FORMAT, &formatInfo.nFormatID, sizeof(formatInfo.nFormatID)) == IS_SUCCESS);

        int width = formatInfo.nWidth;
        int height = formatInfo.nHeight;

        RUNTIME_ASSERT(is_AllocImageMem(hCam, width, height, 8, &pcImageMem, &id) == IS_SUCCESS);
        //RUNTIME_ASSERT(is_SetAllocatedImageMem(hCam, width, height, 8, pcImageMem, &id) == IS_SUCCESS);
        RUNTIME_ASSERT(is_SetImageMem(hCam, pcImageMem, id) == IS_SUCCESS);

        // Freerunning mode
        if (is_SetExternalTrigger(hCam, IS_GET_EXTERNALTRIGGER) != IS_SET_TRIGGER_OFF)
            RUNTIME_ASSERT(is_SetExternalTrigger(hCam, IS_SET_TRIGGER_OFF) == IS_SUCCESS);

        // TODO: Auto exposure
        //is_PixelClock();
        //is_SetFrameRate();
        //is_Exposure();

        RUNTIME_ASSERT(is_CaptureVideo(hCam, IS_DONT_WAIT) == IS_SUCCESS);

        // Process Frame Events
        RUNTIME_ASSERT(is_EnableEvent(hCam, IS_SET_EVENT_FRAME) == IS_SUCCESS);
        while (true) {
            RUNTIME_ASSERT(is_WaitEvent(hCam, IS_SET_EVENT_FRAME, INFINITE) == IS_SUCCESS);

            cv::Mat image(height, width, CV_8UC1);
            for (int i = 0; i < height; ++i) {
                for (int j = 0; j < width; ++j) {
                    image.at<uchar>(i, j) = pcImageMem[i * width + j];
                }
            }

            cv::imshow("Frame", image);
            cv::waitKey(1);
        }
    } catch (...) {
        eptr = std::current_exception();
    }

    is_DisableEvent(hCam, IS_SET_EVENT_FRAME);
    if (!pcImageMem) is_FreeImageMem(hCam, pcImageMem, id);

    is_ExitCamera(hCam);
    if (eptr) std::rethrow_exception(eptr);

    return 0;
}
*/
