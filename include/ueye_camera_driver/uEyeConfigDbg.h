
#include <string>

// TODO Same as in PL-SLAM

namespace ueye_camera_driver
{
  class uEyeConfig
  {
  public:
      uEyeConfig();
      uEyeConfig(const std::string& config_file);

      ~uEyeConfig();

      void load(const std::string& config_file);

      std::string color_mode; // Stop
      unsigned int pixel_clock; // Stop
      double frame_rate; // In freerun
      bool auto_exposure;
      double exposure_time;
      bool auto_gain;
      int hardware_gain;
      bool gain_boost;
      bool hardware_gamma;
      bool enable_trigger; // Stop
      std::string trigger_mode; // Stop
  };
}